# NOTES

## Changes in python version:

- update all scripts to point to correct python before running pip.
- update activate to point to the new directory

## package installation

- uses default python installation (LCG version after activate)
- on lxplus
- pip install --ignore-installed --prefix `pwd` <package>
- git status

# pip upgrade

- pip install --ignore-installed --prefix `pwd` --upgrade pip

# Library specifics

not on LCG
- crcmod deepdiff "fastapi[all]" prometheus_client pydantic_settings inotify_simple jira netifaces selectors34 subprocess32 supervisor timeout_decorator multidispatch

gives error:

ERROR: pip's dependency resolver does not currently take into account all the packages that are installed. This behaviour is the source of the following dependency conflicts.
qkeras 0.8.0 requires tensorflow-model-optimization>=0.2.1, which is not installed.
dask-kubernetes 2022.5.2 requires kopf>=1.35.3, which is not installed.
tensorboard 2.8.0 requires tensorboard-data-server<0.7.0,>=0.6.0, which is not installed.
tensorflow 2.8.0 requires libclang>=9.0.1, which is not installed.
tensorflow 2.8.0 requires tensorflow-io-gcs-filesystem>=0.23.1, which is not installed.
tensorflow 2.8.0 requires tf-estimator-nightly==2.8.0.dev2021122109, which is not installed.
archspec 0.1.2 requires click<8.0,>=7.1.2, but you have click 8.0.3 which is incompatible.
coffea 0.7.13 requires numpy<1.22,>=1.16.0, but you have numpy 1.22.3 which is incompatible.
distributed 2022.6.1 requires locket>=1.0.0, but you have locket 0.2.1 which is incompatible.
globus-sdk 2.0.1 requires pyjwt[crypto]<2.0.0,>=1.5.3, but you have pyjwt 2.0.1 which is incompatible.
kubernetes 23.3.0 requires pyyaml>=5.4.1, but you have pyyaml 5.3.1 which is incompatible.
numba 0.51.2 requires llvmlite<0.35,>=0.34.0.dev0, but you have llvmlite 0.36.0 which is incompatible.
parsl 1.2.0 requires typeguard>=2.10, but you have typeguard 2.7.0 which is incompatible.
httpstan 4.4.2 requires lz4<4.0,>=3.1, but you have lz4 4.0.0 which is incompatible.
statsmodels 0.13.2 requires patsy>=0.5.2, but you have patsy 0.5.1 which is incompatible.

on LCG, still installed here
- colorama
- neobolt
